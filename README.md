BEdita modules
================

This role's duty is to set up multiple [BEdita](http://www.bedita.com) modules.

Variables
---------

### `bedita_modules`
A list of dictionaries of setup configuration for each module. Each dictionary has:

- a **required** key `key`, to uniquely identify the module
- a **required** key `repository`, to specify the Git repository to clone
- an *optional* key `version`, to tell which branch/tag/commit should be cloned (by default, `HEAD` is used)
- an *optional* key `root`, the directory where the project should be cloned (by default, `{{ bedita_root }}/modules` is used — please, consider **not using** this setting!)

Example
-------

```yaml
---
bedita_modules:
  - key: tickets
    repository: https://github.com/bedita/tickets.git
```
